package com.supaprapat.taskmgmtapi.dao

import java.sql._

import com.supaprapat.taskmgmtapi.configuration.Configuration
import com.supaprapat.taskmgmtapi.model._

import scala.slick.jdbc.meta.MTable
import scala.slick.driver.MySQLDriver.simple.Database.threadLocalSession
import scala.slick.driver.MySQLDriver.simple._


/**
  * Created by Supaprapat on 3/11/2017.
  */
class TaskDAO extends Configuration {
  private val db = Database.forURL(url = "jdbc:mysql://%s:%d/%s".format(dbHost, dbPort, dbName),
    user = dbUser, password = dbPassword, driver = "com.mysql.jdbc.Driver")

  db.withSession{
    if(MTable.getTables("tasks").list().isEmpty){
      Tasks.ddl.create
    }
  }

  def create(task: Task): Either[Failure, Task] = {
    try {
      val id = db.withSession {
        Tasks returning Tasks.id insert task
      }
      Right(task.copy(id = Some(id)))
    } catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  def get(id: Long): Either[Failure, Task] = {
    try {
      db.withSession{
        Tasks.findById(id).firstOption match {
          case Some(task: Task) => Right(task)
          case _ => Left(notFoundError(id))
        }
      }
    } catch {
      case e: SQLException => Left(databaseError(e))
    }
  }

  def get(): Either[Failure, List[Task]] = {
    try {
      db.withSession{
        val query = for {
          task <- Tasks
        } yield task
        Right(query.run.toList)
      }
    } catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  def update(id: Long, task: Task): Either[Failure, Task] = {
    try{
      db.withSession{
        Tasks.where(_.id === id) update task.copy(id = Some(id)) match {
          case 0 => Left(notFoundError(id))
          case _ => Right(task.copy(id = Some(id)))
        }
      }
    }
    catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  def setTaskStatusByPut(id: Long, taskStatusRequest: TaskStatusRequest): Either[Failure, Task] = {
    try {
      db.withSession {
        Tasks.findById(id).firstOption match {
          case Some(task: Task) => {
            val map = Query(Tasks)
              .filter(_.id === id)
              .map(t => t.status)
            if (taskStatusRequest.status.get.equals(0) ||  taskStatusRequest.status.get.equals(1)){
              val statusNum = taskStatusRequest.status.get
              map.update(statusNum)
              Right(Tasks.findById(id).first())
            } else {
              Left(statusInputMismatchError)
            }
          }
          case _ => Left(notFoundError(id))
        }
      }
    }
    catch {
      case e: SQLException =>
        Left(databaseError(e))
    }
  }

  def delete(id: Long): Either[Failure, String]= {
    try{
      db.withTransaction {
        val query = Tasks.where(_.id === id)
        val tasksCount = query.run.toList
        if(tasksCount.isEmpty){
          Left(notFoundError(id))
        }else{
          query.delete
          Right("Deleted")
        }
      }
    } catch {
      case e: SQLException => Left(databaseError(e))
    }
  }

  protected def databaseError(e: SQLException) =
    Failure("%d: %s".format(e.getErrorCode, e.getMessage), FailureType.DatabaseFailure)

  protected def notFoundError(taskId: Long) =
    Failure("Task with id=%d does not exist".format(taskId),FailureType.NotFound)

  protected def statusInputMismatchError() =
    Failure("Task status should be 0 or 1",FailureType.BadRequest)
}

