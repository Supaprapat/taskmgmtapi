package com.supaprapat.taskmgmtapi.main

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import com.supaprapat.taskmgmtapi.configuration.Configuration
import com.supaprapat.taskmgmtapi.service.ServiceActor
import spray.can.Http

/**
  * Created by Dell on 3/11/2017.
  */
object Main extends App with Configuration {

  implicit val system = ActorSystem("taskmgmtapi")
  val restService = system.actorOf(Props[ServiceActor], "rest-endpoint")
  IO(Http) ! Http.Bind(restService, serviceHost, servicePort)

}
