package com.supaprapat.taskmgmtapi.service

import java.text.{ParseException, SimpleDateFormat}
import java.util.Date

import akka.actor.Actor
import akka.event.slf4j.SLF4JLogging
import com.supaprapat.taskmgmtapi.dao.TaskDAO
import com.supaprapat.taskmgmtapi.model.{Failure, Task, TaskStatusRequest}
import net.liftweb.json.{DateFormat, Formats}
import net.liftweb.json.Serialization._
import spray.http._
import spray.httpx.unmarshalling._
import spray.routing._

/**
  * Created by Supaprapat on 3/11/2017.
  */
class ServiceActor extends Actor with RestService  {

  implicit def actorRefFactory = context
  def receive = runRoute(rest)

}

trait RestService extends HttpService with SLF4JLogging {
  val taskService = new TaskDAO
  implicit val executionContext = actorRefFactory.dispatcher

  implicit val liftJsonFormats = new Formats {
    val dateFormat = new DateFormat {
      val sdf = new SimpleDateFormat("yyyy-MM-dd")

      def parse(s: String): Option[Date] = try{
        Some(sdf.parse(s))
      } catch {
        case e: Exception => None
      }

      def format(d: Date): String = sdf.format(d)
    }
  }

  implicit val string2Date = new FromStringDeserializer[Date] {
    def apply(value: String) = {
      val sdf = new SimpleDateFormat("yyyy-MM-dd")
      try Right(sdf.parse(value))
      catch {
        case e: ParseException => {
          Left(MalformedContent("'%s' is not a valid Date value" format (value), e))
        }
      }
    }
  }

  implicit val customRejectionHandler = RejectionHandler {
    case rejections => mapHttpResponse {
      response =>
        response.withEntity(HttpEntity(ContentType(MediaTypes.`application/json`),
          write(Map("error" -> response.entity.asString))))
    } {
      RejectionHandler.Default(rejections)
    }
  }

  val rest = respondWithMediaType(MediaTypes.`application/json`) {
    path("task") {
      post {
        entity(Unmarshaller(MediaTypes.`application/json`) {
          case httpEntity: HttpEntity =>
            read[Task](httpEntity.asString(HttpCharsets.`UTF-8`))
        }) {
          task: Task =>
            ctx: RequestContext =>
              handleRequest(ctx, StatusCodes.Created) {
                log.debug("Create task: %s".format(task))
                taskService.create(task)
              }
        }
      } ~
      get {
        ctx: RequestContext =>
          handleRequest(ctx) {
            log.debug("Retrieving all tasks")
            taskService.get()
          }
      }
    } ~
      path("task" / LongNumber){
        taskId =>
          put {
            entity(Unmarshaller(MediaTypes.`application/json`){
              case httpEntity: HttpEntity =>
                read[Task](httpEntity.asString(HttpCharsets.`UTF-8`))
            }) {
              task: Task =>
                ctx: RequestContext =>
                  handleRequest(ctx) {
                    log.debug("Updating task id %d: %s".format(taskId, task))
                    taskService.update(taskId, task)
                  }
            }
          } ~
          delete {
            ctx: RequestContext =>
              handleRequest(ctx) {
                log.debug("Deleting task id %d".format(taskId))
                taskService.delete(taskId)
              }
          } ~
          get {
            ctx: RequestContext =>
              handleRequest(ctx) {
                log.debug("Retrieving task id %d".format(taskId))
                taskService.get(taskId)
              }
          }

      } ~
      path("task" / LongNumber / "setStatus"){
        taskId =>
          put {
            entity(Unmarshaller(MediaTypes.`application/json`){
              case httpEntity: HttpEntity =>
                read[TaskStatusRequest](httpEntity.asString(HttpCharsets.`UTF-8`))
            }) {
              taskStatusRequest: TaskStatusRequest =>
                ctx: RequestContext =>
                  handleRequest(ctx) {
                    log.debug("Updating task status of task id %d: %s".format(taskId, taskStatusRequest))
                    taskService.setTaskStatusByPut(taskId, taskStatusRequest)
                  }
            }
          }
      }
  }

  protected def handleRequest(ctx: RequestContext, successCode: StatusCode = StatusCodes.OK)(action: => Either[Failure, _]): Unit ={
    action match {
      case Right(result: Object) =>
        ctx.complete(successCode, write(result))
      case Left(error: Failure) =>
        ctx.complete(error.getStatusCode, net.liftweb.json.Serialization.write(Map("error" -> error.message)))
      case _ =>
        ctx.complete(StatusCodes.InternalServerError)
    }
  }

}
