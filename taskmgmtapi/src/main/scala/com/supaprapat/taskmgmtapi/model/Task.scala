package com.supaprapat.taskmgmtapi.model

import scala.slick.driver.MySQLDriver.simple._

/**
  * Created by Supaprapat on 3/11/2017.
  */
case class Task(id: Option[Long], title: String, detail: String, status: Option[Int])

object Tasks extends Table[Task]("tasks"){

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def title = column[String]("title")
  def detail = column[String]("detail",O.Nullable)
  def status = column[Int]("status")

  def * = id.? ~ title ~ detail ~ status.? <>(Task,Task.unapply _)

  val findById = for {
    id <- Parameters[Long]
    c <- this if c.id is id
  } yield c

}

case class TaskStatusRequest(id: Option[Long], status: Option[Int])