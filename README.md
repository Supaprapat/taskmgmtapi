**Task Management API**

Prerequisites
- jdk1.8.0_102
- MySQL Database version 5.6.26 or higher
- IntelliJ IDEA with scala and SBT plugins installed

How to setup
1. Create database with following detail in MySQL:
`
1.1 Database name: taskdb
1.2 Database port: 3306
1.3 Database user: task
1.4 Database password: password
`
2. Import project to IntelliJ IDEA
3. Build project
4. Go to src/main/scala/com.supaprapat.taskmgmtapi.main/Main and right click > 'Run Main' to run the program

API Document
1. View all tasks
    - URL : /task/
    - Method : GET
    - Response Example :
    `
                {
                  "id": 1,
                  "title": "task 1",
                  "detail": "detail 1",
                  "status": 0
                },
                {
                  "id": 2,
                  "title": "task 2",
                  "detail": "detail 2",
                  "status": 0
                }
                `
    - Response Codes : 200 OK, 404 Not Found

2. View a single task
    - URL : /task/:id
    - Method : GET
    - URL Params :  Optional: id=[integer]
    - Response Example :
                `
                {
                  "id": 1,
                  "title": "task 1",
                  "detail": "detail 1",
                  "status": 0
                }
                `
    - Response Codes : 200 OK, 404 Not Found 

3. Add a task
    - URL : /task/
    - Method : POST
    - Data Params : Example:
                `
                {
                  "title": "task 7",
                  "detail": "detail 7",
                  "status": 0
                }
                `
    - Response Example :
                `
                {
                  "id": 7,
                  "title": "task 7",
                  "detail": "detail 7",
                  "status": 0
                }
                `
    - Response Codes : 201 Created, 500 Internal Server Error

4. Edit existing task
    - URL : /task/:id
    - Method : PUT
    - URL Params :  Required: id=[integer]
    - Data Params : Example:
                `
                {
                  "title": "task 7",
                  "detail": "detail 7",
                  "status": 0
                }
                `
    - Response Example :
                `
                {
                  "id": 7,
                  "title": "task 7",
                  "detail": "detail 7",
                  "status": 0
                }
                `
    - Response Codes : 200 OK, 400 Bad Request

5. Set task status
    - URL : /task/:id/setStatus
    - Method : PUT
    - URL Params :  Required: id=[integer]
    - Data Params : Status should be 0 or 1
        Example:
                `
                {
                  "status": 1
                }
                `
    - Response Example :
                `
                {
                  "id": 7,
                  "title": "task 7",
                  "detail": "detail 7",
                  "status": 1
                }
                `
    - Response Codes : 200 OK, 400 Bad Request

6. Delete a task
    - URL : /task/:id
    - Method : DELETE
    - URL Params :  Required: id=[integer]
    - Response Example :
               ` "Deleted"`
    - Response Codes : 200 OK, 404 Not Found 
